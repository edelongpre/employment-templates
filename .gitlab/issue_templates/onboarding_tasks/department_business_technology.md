#### Business Technology 

<details>
<summary>Manager/Lis Vinueza @lisvinueza</summary>

1. [ ] Invite to meetings: 
    *  [ ]  All Roles: Monthly Finance, bi-weekly Business Technology meeting
    *  [ ]  Enterprise Applications Team: Invite to Weekly sync, Daily office hours
    *  [ ]  Team Member Enablement (IT) Team: Weekly meeting
1. [ ] Invite to channels:
    * [ ] #bt-confidential
    * [ ] #finance-confidential
1. [ ] Invite to groups
    * [ ] businesstechnology@ google group

</details>

<details>
<summary>New Team Member</summary>

1. [ ] Join Slack channels: #finance, #business-technology, #bt-team-lounge.  
    * [ ]  If you are a BSA: #bt-business-engagement. You can reach out in #bt-business-engagement to the other BSAs to ask which channels you should join based on your area of focus
    * [ ]  If you are part of the Data Team: #data, #data-lounge. Find other channels to join in the [Data Handbook](https://about.gitlab.com/handbook/business-ops/data-team/#slack).
    * [ ]  If you are part of the IT Team: #it_help. Ask your manager to invite you to any private channels.
    * [ ]  If you are part of the Procurement team: #procurement

</details>
