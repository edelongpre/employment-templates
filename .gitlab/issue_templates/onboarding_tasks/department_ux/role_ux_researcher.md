#### UX Researchers

<details>
<summary>New Team Member</summary>

1. [ ] Join the [UX channel]on Slack.
1. [ ] Join the [UX research channel]on Slack.
1. [ ] Join the [UX research team lounge channel] on Slack
1. [ ] Familiarize yourself with the [UX Research pages](https://about.gitlab.com/handbook/engineering/ux/ux-research/) in the handbook.
</details>


<details>
<summary>Manager</summary>

1. [ ] Add new team member to UX Department weekly call.
1. [ ] Add new team member to UX Research weekly call.
1. [ ] Add new team member to UX Hangout calls.
1. [ ] Add new team member to Qualtrics.
1. [ ] Add new team member to Balsamiq.
1. [ ] Add new team member to Mural.
1. [ ] Add new team member to Dovetail.
1. [ ] Give new team member `Developer` access to the [UX Retrospective](https://gitlab.com/gl-retrospectives/ux-retrospectives) project on GitLab.com
1. [ ] Add new team member to the [UX Research Team issue board](https://gitlab.com/groups/gitlab-org/-/boards/1540253).
1. [ ] Announce new team member's arrival in UX Department weekly call.
1. [ ] Announce new team member's arrival in the Engineering week in review.

</details>
