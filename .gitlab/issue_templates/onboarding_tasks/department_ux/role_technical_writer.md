#### Technical Writers

<details>
<summary>Manager</summary>

1. [ ] Create a confidential [Docs Team Onboarding issue](https://gitlab.com/gitlab-org/technical-writing/issues/new) in the <https://gitlab.com/gitlab-org/technical-writing> project.
   1. [ ] Assign issue to the new team member.
   1. [ ] Begin completing the "Manager" tasks.

</details>
