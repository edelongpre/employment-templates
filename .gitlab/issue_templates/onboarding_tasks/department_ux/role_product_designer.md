#### Product Designers

<details>
<summary>New Team Member</summary>

1. [ ] Join the [#ux](https://gitlab.slack.com/messages/ux/) channel on Slack.
1. [ ] Familiarize yourself with the [UX Designer Onboarding](https://about.gitlab.com/handbook/engineering/ux/uxdesigner-onboarding/) page and [relevant pages](https://about.gitlab.com/handbook/engineering/ux/uxdesigner-onboarding/#relevant-links) linked from there.
1. [ ] Your specific UX team buddy is: (FILL IN WITH @ HANDLE).

</details>

<details>
<summary>Manager</summary>

1. [ ] An access request will be automatically created for your new team member on their second day using the [Product Designer Access Request template](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/blob/master/.gitlab/issue_templates/role_baseline_access_request_tasks/department_ux/role_product_designer.md). This includes:
    1. `@uxers` slack handle
    1. `ux-department@gitlab.com` email
    1. `@gitlab\\-com/gitlab\\-ux` group
    1. dev.gitlab.com
1. [ ] Give new team member `Developer` access to the [UX Retrospective](https://gitlab.com/gl-retrospectives/ux-retrospectives) project on GitLab.com.
1. [ ] Give new team member `Developer` access to the [GitLab UX](https://gitlab.com/gitlab-com/gitlab-ux) group on GitLab.com.
1. [ ] Add new team member to the designer and stage group [mention groups](https://gitlab.com/gitlab-com/gitlab-ux) for mentioning in GitLab.
1. [ ] Share the UX calendar with new team member.
1. [ ] Add new team member to UX weekly calls.
1. [ ] Add new team member to relevant [product categories](https://about.gitlab.com/handbook/product/categories/).

</details>

<details>
<summary>UX Foundation Manager</summary>

1. [ ] UX Foundations: Add new team member to the [Figma Group](https://www.figma.com/files/team/749263947527384695/GitLab/members) with **edit** access.

</details>
<details>
<summary>UX Buddy</summary>

1. [ ] UX buddy: Create onboarding issue in the [GitLab Design](https://gitlab.com/gitlab-org/gitlab-design/issues/new) project on GitLab.com.

</details>

#### Product Design Managers

<details>
<summary>New Team Member</summary>

1. [ ] Join the [#ux](https://gitlab.slack.com/messages/ux/) channel on Slack.
1. [ ] Your specific UX team buddy is: (FILL IN WITH @ HANDLE).

</details>

<details>
<summary>Manager</summary>

1. [ ] An access request will be automatically created for your new team member on their second day using the [Product Designer Access Request template](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/blob/master/.gitlab/issue_templates/role_baseline_access_request_tasks/department_ux/role_product_designer.md). This includes:
	1. `@uxers` slack handle
	1. `ux-department@gitlab.com` email
	1. `@gitlab\\-com/gitlab\\-ux` group
	1. dev.gitlab.com
1. [ ] Add new team member to Mural.
1. [ ] Give new team member `Maintainer` access to the [UX Retrospective](https://gitlab.com/gl-retrospectives/ux-retrospectives) project on GitLab.com.
1. [ ] Give new team member `Maintainer` access to the [GitLab UX](https://gitlab.com/gitlab-com/gitlab-ux) group on GitLab.com.
1. [ ] Add new team member to the designer and stage group [mention groups](https://gitlab.com/gitlab-com/gitlab-ux) for mentioning in GitLab.
1. [ ] Share the UX calendar with new team member.
1. [ ] Add new team member to UX weekly calls.
1. [ ] Add new team member to relevant [product categories](https://about.gitlab.com/handbook/product/categories/) by following the directions [here](https://about.gitlab.com/handbook/marketing/inbound-marketing/digital-experience/website/#updating-responsible-persons-for-a-group).

</details>

<details>
<summary>UX Buddy</summary>

1. [ ] UX buddy: Create a `Product Design Manager Onboarding` issue in the [GitLab Design](https://gitlab.com/gitlab-org/gitlab-design/issues/new) project on GitLab.com.
<details>


<summary>UX Foundation Manager</summary>

1. [ ] UX Foundations: Add new team member to the [Figma Group](https://www.figma.com/files/team/749263947527384695/GitLab/members) with **edit** access.

</details>
