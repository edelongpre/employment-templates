#### Infrastructure Department

<details>
<summary>New Team Member</summary>

1. [ ] Familiarize yourself with the [Infrastructure handbook](https://about.gitlab.com/handbook/engineering/infrastructure/) and [Engineering function handbook](https://about.gitlab.com/handbook/engineering).
1. [ ] Join the channels listed in the [keeping yourself informed engineering section](https://about.gitlab.com/handbook/engineering/#sts=Keeping%20yourself%20informed)
1. [ ] Review your team [handbook page] (https://about.gitlab.com/handbook/engineering/infrastructure/team/) and look for additional onboarding steps.
</details>

<details>
<summary>Manager</summary>

1. [ ] Create a new onboarding checklist based on your team onboarding template. 
   * [SRE](https://gitlab.com/gitlab-com/gl-infra/infrastructure/blob/master/.gitlab/issue_templates/onboarding_template.md).
   * Delivery
   * [Scalability](https://gitlab.com/gitlab-com/gl-infra/scalability/-/blob/master/.gitlab/issue_templates/Onboarding.md)
</details>
