### Day 1 - For Team Members in New Zealand only

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Please complete and sign the [IR330 form](https://www.ird.govt.nz/-/media/project/ir/home/documents/forms-and-guides/ir300---ir399/ir330/ir330-2019.pdf). 
2. [ ] New team member: Once completed, please upload in BambooHR under 'Employee Uploads' and make a comment in the issue and tag Harley Devlin or Nicole Precilla (Non US Payroll). 

</details>

