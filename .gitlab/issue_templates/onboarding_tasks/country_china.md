### For team members in China

<details>
<summary>People Experience</summary>

1. [ ] People Experience: Double check that the `Employment Status` in BambooHR has three entries:   
   * Effective Date: `Hire Date`  Employment Status: `Probationary Period`  Comment: `3-month Probationary Period until YYYY-MM-DD (3 months after hire date)` 
   * Effective Date: `3 months after Hire Date` Employment Status: `End of Probation Period` Comment: End of Probationary Period
   * Effective Date: `3 months + 1 day after Hire Date` Employment Status: `Active` Comment: No need to comment.

</details>
