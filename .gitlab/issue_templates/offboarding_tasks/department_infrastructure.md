### FOR INFRASTRUCTURE TEAM MEMBERS ONLY

- [ ] @jurbanc: Remove the team member from Tenable.IO
- [ ] @dawsmith @albertoramos: Remove the team member from Chef
- [ ] @dawsmith @albertoramos: Remove the team member from Cloudflare
- [ ] @dawsmith @albertoramos: Remove the team member from Fastly CDN
- [ ] @dawsmith @albertoramos: Remove the team member from PackageCloud
- [ ] @dawsmith @albertoramos: Remove the team member from Status - IO
- [ ]   Google Search Console @darawarde @sdaily: Remove the team member from Google Search Console

